# frozen_string_literal: true

run lambda { |_env|
  token = File.read(Dir.pwd + '/data/' + 'token.txt').strip!

  [200, { 'Content-Type' => 'text/plain', 'Gitlab-On-Demand-DAST' => token }, [token]]
}
