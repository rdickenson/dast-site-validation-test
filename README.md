# `dast-site-validation-test`

A tiny web app that serves a text file's contents in the response body and
`Gitlab-On-Demand-DAST` header in order to make local testing of DAST site
validation easier during development.

## Dependencies

- `docker`

## Useage

### Basic

To build and start the web app run:

```bash
./dast-site-validation-test start
```

To stop the web app run:

```bash
./dast-site-validation-test stop
```

### Flow

- Start web app and copy `ngrok` url (https://*.ngrok.io) for validation
- Obtain `dast_site_token` from the monolith
- Put contents inside `data/token.txt`
- Verify using either `TEXT_FILE` or `HEADER` validation strategies

You also can watch [this video](https://www.youtube.com/watch?v=1BqekF5kGC8) 
if you find consuming information in this way more accessible.

#### Notes

You will need to enable two feature flags locally to use these features:

```bash
echo "Feature.enable(:security_on_demand_scans_site_validation); Feature.enable(:security_on_demand_scans_http_header_validation)" | bundle exec rails c
```
